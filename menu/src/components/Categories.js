import './Menu.css'
import React from 'react';
import Category from './Category';

class Categories extends React.Component {
    onCategoryClick = event => {
        event.preventDefault();
        this.props.onClick(event.target.getAttribute("data-categories"));      
    };

    render() {
        const categories = this.props.categories.map((category, index) => {
        return <Category key={category.id} index={index} isSelected={index == this.props.selectedCategory} category={category}/>
    })
        return (
            <div className="category-header" onClick={this.onCategoryClick}>{categories}</div>
        );

    }
}



export default Categories;