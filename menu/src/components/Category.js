import './Menu.css'
import React from 'react';

class Category extends React.Component {
    render() {
        const { name } = this.props.category;
        const { index, isSelected } = this.props;
        var classNameExt = isSelected ? 'highlight':'';
        return (
        <div className = {'category ui button ' + classNameExt} data-categories={index}>
            {name}
        </div>
        );
    }
}



export default Category;