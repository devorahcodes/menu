import './Menu.css'
import React from 'react';

class Item extends React.Component {
    
    render() {
        const { name, description } = this.props.item;
        const uri_name = encodeURI(name);
        
        return (
        <div className = "item">
            <div className = "item-text">
                {name}
            </div>
            <div className = "item-image">
                <img src={`https://via.placeholder.com/150.png?text=${uri_name}`} alt={name}/>
            </div>
        </div>
        );
    }
}



export default Item;