import './Menu.css'
import React from 'react';

class Group extends React.Component {

    render() {
        const { name } = this.props.group;
        const { index, isSelected } = this.props;
        var classNameExt = isSelected ? 'highlight':'';
        return (
        <div className = {'menu-group ui button ' + classNameExt} data-group={index}>
            {name}
        </div>
        );
    }
}

export default Group;