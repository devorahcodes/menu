import './Menu.css'
import React from 'react';
import Item from './Item';

const Items = (props) => {
  const items = props.items.map((item) => {
     return <Item key = {item.id} item={item}/>
})
    return <div className="items">{items}</div>
};

export default Items;