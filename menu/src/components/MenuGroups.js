import './Menu.css'
import React from 'react';
import Group from './Group';

class MenuGroups extends React.Component {
    onGroupClick = event => {
        event.preventDefault();
        this.props.onClick(event.target.getAttribute("data-group"));
    };

    render() {
        const groups = this.props.groups.map((group, index) => {
         return <Group key={group.id} index={index} isSelected={index == this.props.selectedMenu} group={group}/>})
        return (
            <div className="menu-header" onClick={this.onGroupClick}>{groups}</div>
        );
    }
}



export default MenuGroups;