import React from "react";

import MenuGroups from "./MenuGroups";
import Categories from "./Categories"
import Items from "./Items"

class App extends React.Component {
  state = {
      loading: true,
      menu: null,
      categories: null,
      items: null,
      selectedMenu: 0,
      selectedCategory: 0
    };

  async componentDidMount() {
    const url = "/api/menu";
    const response = await fetch(url);
        
    const data = await response.json();
    console.log(data);    
    
    this.setState({ menu: data.data.MenuGroups, categories: data.data.MenuGroups[0].categories, 
    items: data.data.MenuGroups[0].categories[0].items,
    loading: false });
  }
    
  onGroupClick = async (index) =>{
    if(index == null) return;
    this.setState({ 
        menu: this.state.menu, 
        categories: this.state.menu[index].categories,
        items: this.state.menu[index].categories[0].items,
        loading: false, 
        selectedMenu: index,
        selectedCategory: 0
      });
  }

  onCategoryClick = async (index) =>{
    if(index == null) return;
    this.setState({ 
        menu: this.state.menu, 
        categories: this.state.menu[this.state.selectedMenu].categories,
        items: this.state.menu[this.state.selectedMenu].categories[index].items, 
        loading: false, 
        selectedMenu: this.state.selectedMenu,
        selectedCategory: index,
      });
  }

  render (){
    if (this.state.loading) {
        return <div>loading...</div>;
      }

    if (!this.state.menu) {
      return <div>No menu</div>;
    }
    return (

    <div>
      <MenuGroups 
      groups = {this.state.menu}
      selectedMenu = {this.state.selectedMenu}
      onClick={this.onGroupClick}   
      />
      <Categories 
      categories = {this.state.categories}
      selectedCategory = {this.state.selectedCategory}
      onClick={this.onCategoryClick}   
      />
      <div className="items-body">
        <Items 
        items = {this.state.items}
        />
      </div> 
      
     </div> 
    );
    
 }
}

export default App;
